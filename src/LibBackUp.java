import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LibBackUp {
    public void addCommitandPush(String repoPath){
        ProcessBuilder processBuilder = new ProcessBuilder();
        // for Linux only
        processBuilder.command("/bin/bash","-c", "cd "+repoPath+" && git add . && git commit -m \"$(date)\"");

        try {

            Process process = processBuilder.start();

            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            int exitCode = process.waitFor();
            System.out.println("\nExited with error code : " + exitCode);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
    }

}
