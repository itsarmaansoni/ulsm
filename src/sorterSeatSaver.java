
class sorterSeatSaver {
	static void swap(int[] hours, int i, int j) {
		int temp = hours[i];
		hours[i] = hours[j];
		hours[j] = temp;

	}

	static int partition(int[] hours,int[] seatArray, int low, int high) {
		int pivot = hours[high];
		int i = (low - 1);

		for (int j = low; j <= high - 1; j++) {
			if (hours[j] < pivot) {
				i++;
				swap(hours, i, j);
                                swap(seatArray,i,j);
			}
		}
		swap(hours, i + 1, high);
                swap(seatArray,i+1,high);
		return (i + 1);
	}

	static void quickSort(int[] hours,int[] seatArray, int low, int high) {
		if (low < high) {
			int pi = partition(hours,seatArray, low, high);
			quickSort(hours,seatArray,low, pi - 1);
			quickSort(hours,seatArray, pi + 1, high);
		}
	}

	public static void printhours(int[] hours) {
		for (int i = 0; i < hours.length; i++) {
			System.out.print(hours[i] + " ");
		}
	}

	public static void main(String[] args) {
		int[] hours = { 10, 7, 8, 9, 1, 5 };
                int[] seatArray = {1,2,3,4,5,6};
		int N = hours.length;
		quickSort(hours,seatArray, 0, N - 1);
		System.out.println("Sorted hoursay:");
		printhours(hours);
	}
}