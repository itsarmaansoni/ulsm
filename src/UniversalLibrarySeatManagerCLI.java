/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */



/**
 *
 * @author life
 */
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class UniversalLibrarySeatManagerCLI {
    ArrayList<Seat> seats= new ArrayList<Seat>();
    ArrayList<Seat> seatsSorted = new ArrayList<Seat>();
    int totalEntries;
    int hrs3,hrs6,hrs9,hrs12,hrs24;
    int hours[] = new int[66];
    int hoursCopy[] = new int[66];
    int seatsSaver[] = new int[66];
    LibBackUp libBackUp = new LibBackUp();
    public void seatInitialize(String filePath){
//        for (int i=0;i<100;i++){
//            seats.add(new Seat(i+1,new Student("",0,"","")));
//        }
        seats.clear();
        seatsSorted.clear();
        for (int i=0;i<66;i++){
            hoursCopy[i]=0;
            hours[i]=0;
            seatsSaver[i]=i+1;
        }
        totalEntries=0;
        hrs3=0;
        hrs6=0;
        hrs9=0;
        hrs12=0;
        hrs24=0;
        ArrayList<String> rawSeatData = new ArrayList<String>();
        rawSeatData = fileToStringLinesArray(filePath);
        for (int i=0;i<rawSeatData.size();i++){
            StringTokenizer tokenizer = new StringTokenizer(rawSeatData.get(i),",");
            if (tokenizer.countTokens()==5){
                int sn = Integer.parseInt(tokenizer.nextToken());
                String nm = tokenizer.nextToken();
                int hrs = Integer.parseInt(tokenizer.nextToken());
                String enTm = tokenizer.nextToken();
                String exTm = tokenizer.nextToken();
//                System.out.println(sn+nm+hrs+enTm+exTm);
                seats.add(new Seat(sn, new Student(nm, hrs, enTm, exTm)));
                hours[sn-1]+=hrs;
                hoursCopy[sn-1]+=hrs;
                totalEntries++;
                if(hrs==3)
                    hrs3++;
                else if (hrs==6)
                    hrs6++;
                else if (hrs==9)
                    hrs9++;
                else if (hrs==12)
                    hrs12++;
                else if(hrs==24)
                    hrs24++;   
            }
//            System.out.println(seats.get(0).students.get(0).name);
        }
        sorterSeatSaver.quickSort(hours, seatsSaver, 0, hours.length-1);
        for (int i=1;i<=66;i++){
            for (int j=0;j<seats.size();j++){
                if(seats.get(j).seatNumber==i){
                    seatsSorted.add(seats.get(j));
                }
            }
        }
        
    }
    
    public void seatListToFile(String filePath){
        try{
            FileWriter fw = new FileWriter(filePath);
            for(int i=0;i<seatsSorted.size();i++){
                for (int j=0;j<seatsSorted.get(i).students.size();j++){
                    String toWrite = seatsSorted.get(i).seatNumber+","+seatsSorted.get(i).students.get(j).name+","+seatsSorted.get(i).students.get(j).hours+","+seatsSorted.get(i).students.get(j).entryTime+","+seatsSorted.get(i).students.get(j).exitTime+"\n";
                    fw.write(toWrite);
                }
            }
            fw.close();
        }
        catch(IOException e){
            System.out.println(e);
        }
    }
    public ArrayList<String> fileToStringLinesArray(String filePath) {
        ArrayList<String> LineArr = new ArrayList<String>();
        try {
            FileReader reader = new FileReader(filePath);
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                LineArr.add(line);
            }
            br.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        return LineArr;
    }
    public ArrayList<Seat> getSeatInfo(int seatNumber){
        ArrayList<Seat> seatData = new ArrayList<Seat>();
        for (int i=0;i<seatsSorted.size();i++){
            if (seatsSorted.get(i).seatNumber == seatNumber){
                seatData.add(seatsSorted.get(i));
            }
        }
        return seatData;
    }
    public void deleteSeatInfoAt(int position,int SeatNumber,String filep,String repoPath){
        int count=0;
        for (int i=0;i<seatsSorted.size();i++){
            if(seatsSorted.get(i).seatNumber==SeatNumber){
                count++;
                if(count==position){
                    seatsSorted.remove(i);
                    break;
                }
            }
        }
        seatListToFile(filep);
        seatInitialize(filep);
        libBackUp.addCommitandPush(repoPath);
    }
    public void addSeatInfo(int seatNumber,String name,int hrs, String entryT, String exitT,String filep,String repoPath){
        seatsSorted.add(new Seat(seatNumber,new Student(name,hrs,entryT,exitT)));
        seatListToFile(filep);
        seatInitialize(filep);
        libBackUp.addCommitandPush(repoPath);
    }
    public static void main(String[] args) {
        UniversalLibrarySeatManagerCLI obj = new UniversalLibrarySeatManagerCLI();
        obj.seatInitialize("/home/life/Documents/LibrarySeatData");
        
        for (int i=0;i<obj.seatsSorted.size();i++){
            System.out.println("Entry number :"+i);
            for(int j=0;j<obj.seatsSorted.get(i).students.size();j++){
                System.out.println("Student seat;"+obj.seatsSorted.get(i).seatNumber);
                System.out.println("Student name:"+obj.seatsSorted.get(i).students.get(j).name);
                System.out.println("Student hours:"+obj.seatsSorted.get(i).students.get(j).hours);
                System.out.println("Student in time:"+obj.seatsSorted.get(i).students.get(j).entryTime);
                System.out.println("Student out time:"+obj.seatsSorted.get(i).students.get(j).exitTime);
            }
        }
        obj.seatListToFile("/home/life/Documents/LibrarySeatData.new");
    }
}
