# ULSM
ULSM is a standalone application that was created to manage seat occupancy details and visualize the heatmap of Universal Library, it also creates a database file that can store all the seat details, including the name, hours, and other misc details of a student

## Installation
you will require [Java](https://www.oracle.com/in/java/technologies/downloads/) and [Ant](https://ant.apache.org/manual/install.html) to build and run ULSM.

inside the ULSM directory, type the following command to clean and build
```bash
ant clean jar
```

## running
the above command is going to create ULSM.jar inside the dist folder, you can type the following command to execute it
```bash
java -jar "dist\ULSM.jar"
```
